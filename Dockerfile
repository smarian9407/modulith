# creates a layer from the openjdk:17-alpine Docker image.
FROM openjdk:17-alpine

MAINTAINER smarian9407@gmail.com

# cd /app
WORKDIR /app

# Refer to Maven build -> finalName
ARG JAR_FILE=target/demo-*.jar

# cp target/demo-0.0.1-SNAPSHOT.jar /app/spring-gitlab-ci.jar
COPY ${JAR_FILE} demo.jar

# java -jar /app/msu.jar
CMD ["java", "-jar", "-Xmx1024M", "/app/demo.jar"]

# Make port 8080 available to the world outside this container
EXPOSE 8080